use std::{thread, ptr, env, fs};
use std::time::Duration;
use std::collections::{HashMap};
use std::path::Path;
use super::window::{Location};
use crate::window::{XDisplay};
use libxdo_sys::{xdo_mouse_down, xdo_mouse_up, xdo_new, xdo_free, xdo_move_mouse, xdo_get_window_location};
use serde::{Serialize, Deserialize};
use crate::{config_dirs, FomStartError};

#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub enum MouseButton {
    Left,
    Right,
    Center,
}

impl MouseButton {
    fn as_xbutton(&self) -> i32 {
        match self {
            MouseButton::Left => 1,
            MouseButton::Right=> 3,
            MouseButton::Center => 2,
        }
    }
}

#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub struct Click {
    position: Location,
    button: Option<MouseButton>,
    click_creates_window: Option<bool>,
    click_absolute: Option<bool>,

    //pause before click.  Default 1 second
    pause_sec: Option<u64>,
}

impl Click {
    pub fn click(&self, disp: &XDisplay, win_id: u64) -> Result<u64, FomStartError> {
        let is_absolute = self.click_absolute.unwrap_or(false);
        let button = self.button.as_ref().unwrap_or(&MouseButton::Left);
        let creates_win = self.click_creates_window.unwrap_or(false);

        let mut start_set_opt = None;

        if creates_win {
            start_set_opt = Some(disp.get_top_windows_as_set());
        }

        let xdo = unsafe{ xdo_new(ptr::null()) };

        let abs_pos;
        if is_absolute {
            abs_pos = self.position.clone();
        }
        else {
            let mut wx = 0;
            let mut wy = 0;
            unsafe {xdo_get_window_location(xdo, win_id, &mut wx, &mut wy, ptr::null_mut()); }

            abs_pos = Location{
                x: self.position.x + wx,
                y: self.position.y + wy,
            }
        }

        let pause_sec = self.pause_sec.unwrap_or(1);

        unsafe {
            xdo_move_mouse(xdo, abs_pos.x, abs_pos.y, 0);
            thread::sleep(Duration::from_secs(pause_sec));
            xdo_mouse_down(xdo, 0, button.as_xbutton());
            xdo_mouse_up(xdo, 0, button.as_xbutton());
        }

        unsafe{ xdo_free(xdo); }

        let last_win_id;

        if creates_win {
            let start_set = start_set_opt.unwrap();
            last_win_id = disp.wait_for_new_window(start_set, 30, "click")?;
        }
        else {
            last_win_id = win_id;
        }

        Ok(last_win_id)
    }
}


// #[derive(Clone, Debug, Hash, Serialize, Desereliaze)]
// pub struct ClickMacros {
//
// }

pub type ClickMacros = HashMap<String, Vec<Click>>;


pub fn get_click_macros_from_file(path: &Path) -> Option<ClickMacros> {
    let macro_fname = path.join("click_macros.yaml");
    eprint!("reading {} ... ", macro_fname.to_str().unwrap_or("<unprintable filename>"));
    let read_result = fs::read_to_string(macro_fname);

    if let Ok(data) = read_result {
        serde_yaml::from_str::<ClickMacros>(data.as_str()).ok()
    }
    else {
        None
    }
}

pub fn get_click_macros() -> ClickMacros {

    let mut macros = ClickMacros::new();

    for path in config_dirs() {
        if let Some(m) = get_click_macros_from_file(path.as_path()) {
            macros.extend(m);
            eprintln!("done");
        }
        else {
            eprintln!("failed");
        }
    }

    macros
}