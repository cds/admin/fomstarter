//! Handle startup Yaml script
//! or generate new script from current screen settings.
//!

use std::{fs, path, thread};
use std::collections::{HashSet};
use std::time::Duration;

use serde::{Serialize, Deserialize};
use serde_yaml;
use crate::xdo::XDo;
//use libxdo_sys::{xdo_kill_window};

use self::window::{Window, update_windows, configure_windows};
use self::screen::{ScreenSettings, configure_screen, get_screen};
use crate::FomStartError;
use crate::start::window::diff_windows;
use crate::window::{XDisplay, XWindow};

pub mod screen;
pub mod window;
pub mod click;
pub mod keypress;

#[derive(Debug,Serialize, Deserialize)]
pub struct StartUp {
    screen: Option<ScreenSettings>,
    close_windows: Option<Vec<String>>,
    windows: Vec<Window>,
}

impl StartUp {
    pub fn new () -> StartUp {
        StartUp {
            screen: None,
            close_windows: None,
            windows: Vec::new(),
        }
    }

    pub fn update_windows(other: &Self, new_windows: &Vec<Window>) -> StartUp {
        StartUp {
            screen: other.screen.clone(),
            close_windows: other.close_windows.clone(),
            windows: new_windows.clone(),
        }
    }
}

pub fn open_startup(yaml_name: &str) -> Result<StartUp, FomStartError> {

    // if file doesn't exist, that's ok.  We might just want to write a new one
    let yaml_path = path::Path::new(yaml_name);
    if !yaml_path.exists() {
        eprintln!("*** WARNING yaml file {} does not exist. Treating as empty file.", yaml_name);
        return Ok(StartUp::new());
    }

    //otherwise, try to read it

    let yaml: String;
    let read = fs::read_to_string(yaml_name);

    match read {
        Ok(x) => yaml = x,
        Err(_) => return Err(FomStartError::CantOpenFile(yaml_name.to_string())),
    }

    let start_result = serde_yaml::from_str::<StartUp>(yaml.as_str());
    let starts;
    match start_result {
        Ok(s) => starts = s,
        Err(e) => return Err(FomStartError::CantParseYaml(format!("{}", e))),
    }

    Ok(starts)
}

/// close any window with the given name
fn close_windows(window_names: &Vec<String>) {
    let disp = XDisplay::open_default().unwrap();
    let top_wins = disp.get_top_windows();
    let mut window_set: HashSet<String> = HashSet::new();

    for name in window_names {
        window_set.insert(name.clone());
    }

    let xdo = XDo::new().unwrap();

    let mut any_closed = false;

    for top_win in top_wins {
        let title = xdo.get_window_name(top_win).unwrap();
        if window_set.contains(title.as_str()) {
            eprintln!("Closing window named {}", title);
            XWindow::new(&disp, top_win).unwrap().destroy(&disp);
            any_closed = true;
        }
    }

    drop(disp);

    if any_closed {
        thread::sleep(Duration::from_secs(5));
    }
}


pub fn start(yaml_name: &str) -> Result<(), FomStartError> {
    let starts = open_startup(yaml_name)?;


    // setup the screen
    if let Some(scr) = starts.screen {
        if let Err(e) = configure_screen(&scr) {
            eprintln!("*** ERROR when configuring screens: {}", e);
        }
    }

    // close any windows we should close
    let windows_to_close: Vec<String>;

    match starts.close_windows {
        Some(wins) => windows_to_close = wins.clone(),
        None => windows_to_close = Vec::new(),
    }

    close_windows(&windows_to_close);


    // setup windows
    configure_windows(starts.windows.as_slice())?;

    Ok(())
}

pub fn diff(yaml_name: &str) -> Result<(), FomStartError> {
    let conf_startup = open_startup(yaml_name)?;

    diff_windows(conf_startup.windows.as_slice())?;

    Ok(())
}


/// Update a StartUp Structure with all existing top level screen positions and order.
/// Will preserve clicks.
pub fn generate(yaml_name: Option<& str>, update_only: bool,
                remove_missing: bool, force_stdout: bool,
                include_monitors: bool) -> Result<(), FomStartError> {
    let mut startup: StartUp;

    match yaml_name {
        Some(name) => startup = open_startup(name)?,
        None => startup = StartUp::new(),
    }

    // get screen coords
    let new_screen = if include_monitors {
        match get_screen() {
            Ok(s) => Some(s),
            Err(e) => {
                eprintln!("*** ERROR when querying monitors: {}", e.to_string());
                None
            }
        }
    } else {
        None
    };

    if let Some(s) = new_screen {
        startup.screen = Some(s);
    }


// check windows
    let new_windows = update_windows(startup.windows.as_slice(),
                                         update_only, remove_missing)?;

    let new_startup = StartUp::update_windows(&startup, &new_windows);

    // print yaml to stdout
    let out_yaml = serde_yaml::to_string(&new_startup).unwrap();
    if yaml_name.is_none() || force_stdout {
        println!("{}", out_yaml);
    }
    else {
        let write_result = fs::write(yaml_name.unwrap(), out_yaml);
        if let Err(e) = write_result {
            return Err(FomStartError::CantWriteFile(String::from(yaml_name.unwrap()),
                                                    e.to_string()));
        }
    }

    Ok(())
}