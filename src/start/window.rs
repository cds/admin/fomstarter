use std::cmp::{max, Ordering, PartialEq};
use std::collections::{HashMap};
use std::thread;
use std::process::{Command, Stdio};
use std::time::Duration;
use std::env;
use std::fmt::{Display, Formatter};

use serde::de::{Error, MapAccess};
use serde::{Serialize, Deserialize, Serializer};

use crate::FomStartError;
use crate::window::{XDisplay, XWindow};
use crate::xdo::XDo;
use shell_words;

use pathfinding::kuhn_munkres::kuhn_munkres;
use pathfinding::matrix::Matrix;

use super::click::{Click, get_click_macros};
use super::keypress::{press_keys};


#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub struct Location {
    pub x: i32,
    pub y: i32,
}

impl PartialEq for Location {
    fn eq(&self, other: &Self) -> bool {
        return self.x == other.x && self.y == other.y;
    }
}

impl Display for Location {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub struct Span {
    pub dx: i32,
    pub dy: i32,
}

impl PartialEq for Span {
    fn eq(&self, other: &Self) -> bool {
        return self.dx == other.dx && self.dy == other.dy;
    }
}

impl Display for Span {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.dx, self.dy)
    }
}


#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub struct EnvEntry {
    pub key: String,
    pub value: String,
}


pub struct EnvEntryVisitor {

}

impl<'de> serde::de::Visitor<'de> for EnvEntryVisitor {
    type Value = Option<Vec<EnvEntry>>;

    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        write!(formatter, "key: value environmental variable map")
    }

    fn visit_none<E>(self) -> Result<Self::Value, E> where E: Error {
        Ok(None)
    }

    fn visit_map<A>(self, mut map: A) -> Result<Self::Value, A::Error> where A: MapAccess<'de> {
        let mut v = Vec::new();
        while let Some((key, value)) = map.next_entry()? {
            v.push(EnvEntry{key, value});
        }
        Ok(Some(v))
    }
}

fn deserialize_env_vec<'d, D>(des: D) -> Result<Option<Vec<EnvEntry>>, D::Error>
where
    D: serde::Deserializer<'d>,
{
    des.deserialize_map(EnvEntryVisitor{}).or(Ok(None))
}

fn serialize_env_vec<S>(
    envs_opt: &Option<Vec<EnvEntry>>,
    serializer: S,
) -> Result<S::Ok, S::Error>
where
    S: Serializer
{
    match envs_opt {
        Some(envs) => serializer.collect_map(envs.iter()
            .map(|e| (e.key.clone(), e.value.clone()))),
        None => serializer.serialize_none(),
    }
}


#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub struct Window {
    title: String,
    command: String,
    working_dir: Option<String>,
    #[serde(default)]
    #[serde(deserialize_with = "deserialize_env_vec")]
    #[serde(serialize_with = "serialize_env_vec")]
    environment: Option<Vec<EnvEntry>>,
    position: Location,
    size: Span,
    z_order: u32,
    click_macros: Option<Vec<String>>,
    clicks: Option<Vec<Click>>,
    keypresses: Option<Vec<String>>,
}

impl Window {
    pub fn from_xwindow(xwin: &XWindow) -> Result<Window, FomStartError> {
        let xdo = XDo::new().unwrap();
        let disp = XDisplay::open_default()?;
        let command= xwin.command(& disp).unwrap_or(String::new());
        let trimmed_command = command.trim_end_matches('\0');

        let (x, y) = xwin.get_abs_pos(&disp);

        Ok(Window {
            title: xdo.get_window_name(xwin.win_id).unwrap(),
            command: String::from(trimmed_command),
            working_dir: None,
            position: Location {
                x,
                y,
            },
            size: Span {
                dx: xwin.attributes.width,
                dy: xwin.attributes.height,
            },
            z_order: 0,
            click_macros: None,
            clicks: None,
            environment: None,
            keypresses: None
        })
    }

    pub fn update(&self, other: &Window) -> Window {
        Window {
            title: other.title.clone(),
            command: self.command.clone(),
            working_dir: self.working_dir.clone(),
            position: Location {
                x: other.position.x,
                y: other.position.y
            },
            size: Span {
                dx: other.size.dx,
                dy: other.size.dy,
            },
            z_order: other.z_order,
            click_macros: self.click_macros.clone(),
            clicks: self.clicks.clone(),
            environment: self.environment.clone(),
            keypresses: self.keypresses.clone(),
        }
    }

    pub fn configure(& self, disp: &XDisplay, xwin: &XWindow ) -> Result<(), FomStartError> {
    	// set title
	xwin.set_title(disp, self.title.as_str());



        //adjust size
	xwin.move_resize(disp, self.position.x, self.position.y, u32::try_from(self.size.dx).unwrap_or(0), u32::try_from(self.size.dy).unwrap_or(0));

        Ok(())
    }

    pub fn start(& self, disp: &XDisplay) -> Result<u64, FomStartError> {

        // this is the base-line set used to find new windows
        // when a window appears that's not in this set, we know the program has started.
        let start_set= disp.get_top_windows_as_set();

        // start the process
        let cmd_words;

        let cmd_words_result = shell_words::split(self.command.as_str());
        match cmd_words_result {
            Ok(x) => cmd_words = x,
            Err(pe) => { let em = format!("{}", pe); return Err(FomStartError::BadStartCommand(self.title.clone(), em)); },
        }

        if cmd_words.len() < 1 {
            return Err(FomStartError::BadStartCommand(self.title.clone(), String::from("command was empty")));
        }

        let bin = &cmd_words[0];
        let args = &cmd_words[1..];

        let cwd: &str;

        match &self.working_dir {
            Some(wd) => cwd = wd.as_str(),
            None => cwd = "/tmp",
        }

        let mut cmd_env: HashMap<String, String> = env::vars().collect();

        if let Some(new_envs) =  &self.environment {
            for env_entry in new_envs {
                cmd_env.insert(env_entry.key.clone(), env_entry.value.clone());
            }
        }

        // Command::new(bin).args(args).
        //                 stdout(Stdio::null()).stdin(Stdio::null()).stderr(Stdio::null());


        if let Err(_) = Command::new(bin).args(args).current_dir(cwd).envs(cmd_env).
                            stdout(Stdio::null()).stdin(Stdio::null()).stderr(Stdio::null()).spawn() {
            return Err(FomStartError::BadStartCommand(self.title.clone(), String::from("could not start command")));
        }

        let win_id = disp.wait_for_new_window(start_set, 60, self.title.as_str())?;

        let last_win_id = self.run_clicks(disp, win_id)?;

        Ok(last_win_id)
    }

    pub fn run_clicks(&self, disp :&XDisplay, win_id: u64) -> Result<u64, FomStartError> {

        // run click macros
        let mut last_win_id= win_id;

        if let Some(cm) = self.click_macros.as_ref() {
            let cm_library = get_click_macros();
            for mac_name in cm {
                if cm_library.contains_key(mac_name) {
                    let mclicks = cm_library.get(mac_name).unwrap();
                    for clk in mclicks {
                        thread::sleep(Duration::from_secs(1));
                        last_win_id = clk.click(disp, last_win_id)?;
                    }
                }
                else {
                    eprintln!("*** ERROR: click macro {} was not found", mac_name);
                }
            }
        }

        // run defined clicks
        let clicks;
        match self.clicks.as_ref() {
            Some(x) => clicks = x.clone(),
            None => clicks = Vec::<Click>::new(),
        }

        for click in clicks.iter() {
            thread::sleep(Duration::from_secs(1));
            last_win_id = click.click(disp, last_win_id)?;
        }

        let keypresses: Vec<String>;
        match self.keypresses.as_ref() {
            Some(x) => keypresses = x.clone(),
            None => keypresses = Vec::new(),
        }

        for keypress in keypresses {
            press_keys(last_win_id, keypress.as_str());
        }

        Ok(last_win_id)
    }

    /// return a score indicating how similar two windows settings are
    /// 0 means no similarity.  Higher values indicate higer similarity.
    /// Used for matching windows setting from configuration with windows settings
    /// from open windows
    pub fn similarity_score(&self, other: &Self) -> Result<i32, FomStartError> {
        let title_score = 20;
        let command_score = 20;
        let command_null_score = 5;
        let position_score = 5;
        let size_score = 5;

        let mut score = 0;

        if self.title == other.title {
            score += title_score;
        }

        if self.command == "" {
            score += command_null_score;
        }
        else if other.command == "" {
            score += command_null_score;
        }
        else if compare_commands(self.command.as_str(), other.command.as_str()) {
            score += command_score;
        }

        if self.position == other.position {
            score += position_score;
        }

        if self.size == other.size {
            score += size_score;
        }

        Ok(score)
    }

    /// Compare two window structures.  Print any differences and return an true if there are any
    /// Current settings should be &self and old settings other
    pub fn find_differences(&self, other: &Self) -> bool {
        let mut found_diff = false;
        if self.title != other.title {
            found_diff = true;
            println!("A window title has changed from'{}' to '{}'.", other.title, self.title);
        }
        if self.position != other.position {
            found_diff = true;
            println!("Window {} position has changed from {} to {}", self.title, other.position, self.position);
        }
        if self.size != other.size {
            found_diff = true;
            println!("Window {} size has changed from {} to {}", self.title, other.size, self.size)
        }
        found_diff
    }
 }


/// convert a command string to a vect of strings
/// the first string is the command, the remainder are arguments
fn convert_cmd_to_vec(cmd: &str) -> Result<Vec<String>, FomStartError> {

    if let Some(_) = cmd.find("\0") {
        Ok(cmd.split("\0").map(|s| String::from(s)).collect::<Vec<String>>())
    }
    else {
        match shell_words::split(cmd) {
            Ok(v) => Ok(v),
            Err(pe) => { let em = format!("{}", pe); return Err(FomStartError::BadStartCommand(String::from("comparing commands"), em)); },
        }
    }

}

/// Return true if it can be determined the two cmd strings
/// would start the same command with the same argument
/// This function does not try to unify paths,
/// since in general we can't get a working directory
/// for an window
fn compare_commands(cmd1: &str, cmd2: &str) -> bool {
    let cmd_vec1;
    match convert_cmd_to_vec(cmd1) {
        Ok(v) => cmd_vec1 = v,
        Err(_) => return false,
    }
    let cmd_vec2;
    match convert_cmd_to_vec(cmd2) {
        Ok(v) => cmd_vec2 = v,
        Err(_) => return false,
    }

    if cmd_vec1.len() != cmd_vec2.len() {
        return false;
    }
    else {
        for i in 0..cmd_vec1.len() {
            if cmd_vec1[i] != cmd_vec2[i] {
                return false;
            }
        }
    }
    true
}



/// Match an XWindows ID with a Window object created from the XWindows
/// And an optional reference to a Window setting that's associated with it.
#[derive(Debug, Clone)]
pub struct WindowMapItem<'a> {
    xwindow: XWindow,
    window: Window,
    window_settings: Option<&'a Window>,
}

impl<'a> WindowMapItem<'a> {
    fn from_id(disp: & XDisplay, id: u64) -> Result<WindowMapItem, FomStartError> {
        let xwindow = XWindow::new(disp, id)?;
        let window = Window::from_xwindow(&xwindow)?;

        Ok(
            WindowMapItem {
                xwindow,
                window,
                window_settings: None,
            }
        )
    }
}

pub fn get_top_windows_map(disp: & XDisplay) -> Result<Vec<WindowMapItem>, FomStartError> {

    let top_windows = disp.get_top_windows();

    let mut window_map: Vec<WindowMapItem> = Vec::new();
    for top_win in top_windows.into_iter() {

        let map_item = WindowMapItem::from_id(disp, top_win)?;
        window_map.push(map_item);
    }

    Ok(window_map)
}


/// match setting structs with a particular window map item
/// window_map is a map of open windows
/// window_settings is a slice containing the settings for windows to be configured.
/// the function finds the assignment that maximizes the total similarity score summed
/// over all assignments
/// score for an individual assignment must be above min_score for the assignment to actually happen
///
/// returned vectors of ''unassigned'' window references
pub fn assign_settings_to_window_map<'a>(window_map: &'_ mut [WindowMapItem<'a>], window_settings: &'a [Window], min_score: i32) -> Vec<&'a Window> {
    let rows = window_settings.len();
    let cols = max(window_settings.len(), window_map.len());
    let mut scores: Matrix<i32> = Matrix::new(rows, cols, 0);

    for row in 0..window_settings.len() {
        for col in 0..window_map.len() {
            let window_setting = &window_settings[row];
            let open_win = &window_map[col];
            match window_setting.similarity_score(&open_win.window) {
                Ok(score) => *scores.get_mut((row, col)).unwrap() = score,
                Err(e) => eprintln!("Error comparing windows {} and {}: {}",
                                      window_setting.title, open_win.window.title, e),
            }
        }
    }

    let (_total_score, assignment) = kuhn_munkres(&scores);

    let mut unassigned = Vec::new();

    for row in 0..window_settings.len() {
        let col = assignment[row];

        if col < window_map.len() && *scores.get((row, col)).unwrap() >= min_score {
            window_map[col].window_settings = Some(&window_settings[row]);
        }
        else {
            unassigned.push(&window_settings[row]);
        }
    }
    unassigned
}

/// clean up the list of top windows
/// to remove some windows we don't care about
pub fn get_cleaned_windows(disp: &XDisplay) -> Result<Vec<WindowMapItem>, FomStartError>  {
    let mut map = get_top_windows_map(& disp)?;

    // filter out fomstarter terminal window
    // and some xfce madness
    let mut fomstarter_indices = Vec::new();
    for map_index in 0..map.len() {
        let look_mpi = map.get(map_index).unwrap();
        if let Some(_) = look_mpi.window.title.find("fomstarter") {
            fomstarter_indices.push(map_index);
        }
        else if look_mpi.window.title == "Desktop" || look_mpi.window.title == "xfce4-panel" {
            fomstarter_indices.push(map_index);
        }
        else if look_mpi.window.title.chars().take(9).collect::<String>().as_str() == "controls@" {
            fomstarter_indices.push(map_index);
        }
        else if let Some(_) = look_mpi.window.title.find("@") {
            if let Some(_) = look_mpi.window.title.find(":") {
                fomstarter_indices.push(map_index);
            }
        }
        else if look_mpi.window.title == "medm" {
            fomstarter_indices.push(map_index);
        }
        else if look_mpi.window.title == "MEDM Message Window" {
            fomstarter_indices.push(map_index);
        }
    }

    fomstarter_indices.reverse();

    for map_index in fomstarter_indices {
        map.remove(map_index);
    }

    Ok(map)
}

// Update windows with new positions and sizes.
// Add in any new windows that aren't found there, unless update_only is true
// if remove_missing is true, remove any item from old_windows that doesn't correspond to an open window
pub fn update_windows(old_windows: &  [Window], update_only: bool, remove_missing: bool) -> Result<Vec<Window>, FomStartError> {
    let disp = XDisplay::open_default()?;
    let mut map = get_cleaned_windows(& disp)?;

    // a min score on 11 ensures that a window that matches on position, size, *and* has a null
    // command will match, but any two of those won't.  Set to 6 to match two out of three.
    let unassigned_window_settings = assign_settings_to_window_map(map.as_mut_slice(), old_windows, 11);

    let mut new_windows: Vec<Window> = Vec::new();

    for (i, wmap_item) in map.iter_mut().enumerate() {
        wmap_item.window.z_order = u32::try_from(i).unwrap();

        let display_name = wmap_item.window.title.as_str();

        match wmap_item.window_settings {
            Some(win_setting) => {
                eprintln!("Updating '{}'", display_name);
                let new_win = win_setting.update(&wmap_item.window);
                new_windows.push(new_win);
            },
            None => {
                if update_only {
                    eprintln!("Skipping {}.  Not found in Yaml.", display_name);
                }
                else {
                    eprintln!("Creating new settings for '{};", display_name);
                    new_windows.push(wmap_item.window.clone());
                }
            },
        }
    }

    if !remove_missing {
        for missing_win in unassigned_window_settings {
            new_windows.push(missing_win.clone());
        }
    }

    Ok(new_windows)
}

/// returns an error if there are any differences
pub fn diff_windows(old_windows: &[Window]) -> Result<(), FomStartError> {
    let disp = XDisplay::open_default()?;
    let mut map = get_cleaned_windows(& disp)?;

    // a min score on 11 ensures that a window that matches on position, size, *and* has a null
    // command will match, but any two of those won't.  Set to 6 to match two out of three.
    let unassigned_window_settings = assign_settings_to_window_map(map.as_mut_slice(), old_windows, 11);

    let mut diff_found = false;

    for (i, wmap_item) in map.iter_mut().enumerate() {
        wmap_item.window.z_order = u32::try_from(i).unwrap();

        let display_name = wmap_item.window.title.as_str();

        match wmap_item.window_settings {
            Some(win_setting) => {
                diff_found = wmap_item.window.find_differences(win_setting) || diff_found;
            },
            None => {
                diff_found = true;
                println!("New window {}", wmap_item.window.title);
            },
        }
    }

    for win in unassigned_window_settings {
        diff_found = true;
        println!("Missing window {}", win.title);
    }

    if diff_found {
        Err( FomStartError::DifferenceWithConfig )
    } else {
        Ok(())
    }
}


pub fn configure_windows(window_settings: & [Window] ) -> Result<(), FomStartError>
{
    let disp = XDisplay::open_default()?;
    let mut map = get_top_windows_map(& disp)?;

    // a min score on 11 ensures that a window that matches on position, size, *and* has a null
    // command will match, but any two of those won't.  Set to 6 to match two out of three.
    let unassigned_window_settings = assign_settings_to_window_map(map.as_mut_slice(), window_settings, 11);

    let mut new_wins= Vec::new();
    let mut existing_wins = Vec::new();

    // build up structure indexed by window
    for wmi in map.iter() {
        if let Some(_) = wmi.window_settings {
            existing_wins.push(wmi.clone());
        }
    }

    for wmap in existing_wins.iter() {
        if let Some(ws) = wmap.window_settings {
            eprintln!("Window '{}' already exists as id {}", ws.title, wmap.xwindow.win_id);
        }
    }

    for window in unassigned_window_settings {
        eprint!("Starting window '{}' ...  ", window.title);
        match window.start(& disp) {
            Err(e) => eprintln!("*** WARNING unable to start window {}: {}", window.title, e),
            Ok(win_id) => {
                match  WindowMapItem::from_id(&disp, win_id) {
                    Err(e) => eprintln!("*** ERROR couldn't create XWindows structure for windows {}, ID {}: {}",
                                        window.title, win_id, e),
                    Ok(mut wmi) => {
                        wmi.window_settings = Some(window);
                        //new_win_map.insert(window, wmi);
                        new_wins.push(wmi);
                        eprintln!("created new window as id {}", win_id);
                    }
                }
            },
        }
    }

    if new_wins.len() > 0 {
        eprintln!("waiting for apps to finish loading");
        thread::sleep(Duration::from_secs(30));
    }

    for map_item in existing_wins.iter() {
        new_wins.push(map_item.clone());
    }


    // eprintln!("Configuring pre-existing windows");
    // for (window, map_item) in win_map {
    // 	eprintln!("configuring {}", window.title);
    // 	let mut xwindow: XWindow;
    // 	xwindow = map_item.xwindow.clone();
    //
    // 	window.configure(& disp, &xwindow);
    // }

    eprintln!("Configuring windows");
    for map_item in &new_wins {
        let window = map_item.window_settings.unwrap();
    	eprintln!("configuring {}", window.title);
    	let xwindow = map_item.xwindow.clone();
    	if let Err(e) = window.configure(& disp, &xwindow) {
            eprintln!("Error configuring window {}: {}", window.title, e);
        }
    }

    // set zorder
    new_wins.sort_by(|a, b| {
        let wsa = a.window_settings.unwrap();
        let wsb = b.window_settings.unwrap();
        if wsa.z_order > wsb.z_order {
            return Ordering::Greater;
        }
        else if wsa.z_order < wsb.z_order {
            return Ordering::Less;
        }
        else {
            return Ordering::Equal;
        }
    }
    );

    for map_item in new_wins {
        let xwindow = &map_item.xwindow;
        xwindow.raise(&disp);
    }

    Ok(())
}
