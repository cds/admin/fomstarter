use std::{ptr};

use std::ffi::CString;

use libxdo_sys::{xdo_new, xdo_free, xdo_send_keysequence_window};

pub fn press_keys(win_id: u64, key_sequence: &str) {
    let xdo = unsafe{ xdo_new(ptr::null()) };

    let c_sequence = CString::new(key_sequence).unwrap();

    unsafe{ xdo_send_keysequence_window(xdo, win_id, c_sequence.as_ptr(),
                                        12000);
    }

    unsafe{ xdo_free(xdo); }
}