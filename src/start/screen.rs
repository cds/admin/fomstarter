use super::window::{Span, Location};
use serde::{Serialize, Deserialize};
use xrandr;
use x11::xrandr::Rotation;
use std::collections::HashMap;

use crate::FomStartError;
use crate::window::XDisplay;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Rotate {
    Rotate0Deg,
    Rotate90Deg,
    Rotate180Deg,
    Rotate270Deg,
}

impl Rotate {
    pub fn to_xrandr_rotation(&self) -> Rotation {
        match self {
            Self::Rotate0Deg => 1,
            Self::Rotate90Deg => 2,
            Self::Rotate180Deg => 4,
            Self::Rotate270Deg => 8,
        }
    }

    pub fn from_xrandr_rotation(rot: Rotation) -> Self {
        match rot {
            1 => return Self::Rotate0Deg,
            2 => return Self::Rotate90Deg,
            4 => return Self::Rotate180Deg,
            8 => return Self::Rotate270Deg,
            _ => eprintln!("unrecognized rotate value {}", rot),
        }
        Self::Rotate0Deg
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Monitor {
    name: String,
    position: Location,
    resolution: Span,
    rotate: Rotate,
    refresh: f64,
}

impl Monitor {
    pub fn from_xrandr_monitor(xhandle: & mut xrandr::XHandle, xmon: & xrandr::Monitor) -> Monitor {
        Monitor {
            name: xmon.name.clone(),
            position: Location {
                x: xmon.x,
                y: xmon.y,
            },
            resolution: Span {
                dx: xmon.width_px,
                dy: xmon.height_px,
            },
            rotate: Rotate::from_xrandr_rotation(xmon.get_rotate()),
            refresh: xhandle.get_mode_info(xmon).unwrap().refresh(),
        }
    }

    // update an existing xrandr::Monitor object from this object
    pub fn update_xrandr_monitor(&self, other: &mut xrandr::Monitor) {
        other.x = self.position.x;
        other.y = self.position.y;
        other.width_px = self.resolution.dx;
        other.height_px = self.resolution.dy;
        other.rotate = self.rotate.to_xrandr_rotation();
    }
}

#[derive(Debug,Clone,Serialize, Deserialize)]
pub struct ScreenSettings {
    monitors: Vec<Monitor>,
    display: Span,
    display_mm: Span,
}

/// return ok if hash1's keys are in hash2
/// otherwise, error is the first key that can't be found in hash2
fn subset<T,U>(hash1:  &HashMap<String,T>, hash2:  &HashMap<String,U>) -> Result<(), String> {
    for key in hash1.keys() {
        if !hash2.contains_key(key) {
            return Err(String::from(key));
        }
    }
    Ok(())
}

/// check if all required monitors are there
fn check_monitors<T,U>(wanted_mons: &HashMap<String,T>, real_mons: &HashMap<String,U>) -> Result<(), FomStartError> {
    if let Err(e) = subset(wanted_mons, real_mons) {
        let msg = format!("'{}' was specified in Yaml but not found", e);
        return Err(FomStartError::UnspecifiedMonitors(msg));
    }
    if let Err(e) = subset(real_mons, wanted_mons) {
        let msg = format!("'{}' is connected but was not found in yaml file", e);
        return Err(FomStartError::UnspecifiedMonitors(msg));
    }
    Ok(())
}

pub fn configure_screen(settings: & ScreenSettings) -> Result<(), FomStartError> {

    // hash of desired settings
    let mut settings_hash = HashMap::<String, &Monitor>::new();
    for mon in settings.monitors.iter() {
        settings_hash.insert(mon.name.clone(), mon);
    }

    // hash of real monitors
    let mut real_hash = HashMap::<String, &mut xrandr::Monitor>::new();
    let mut xhandle = xrandr::XHandle::open().unwrap();
    let mut xmonitors = xhandle.monitors().unwrap();
    for xmon in xmonitors.iter_mut() {
        real_hash.insert(xmon.name.clone(), xmon);
    }

    check_monitors(& settings_hash, & real_hash)?;

    for mon in settings.monitors.iter () {
        println!("\nConfiguring monitor {}", mon.name);
        let xmon = real_hash.get_mut(mon.name.as_str()).unwrap();
        mon.update_xrandr_monitor(*xmon);
        let set_result = xhandle.set_monitor(*xmon, mon.refresh);
        if let Err(e) = set_result {
            eprintln!("*** ERROR - unable to configure screen '{}': {}", mon.name, e);
        }
    }

    xhandle.set_screen_size(settings.display.dx, settings.display.dy, settings.display_mm.dx, settings.display_mm.dy);

    Ok(())
}

pub fn get_screen() -> Result<ScreenSettings, FomStartError> {
    let mut xhandle;
    let handle_result = xrandr::XHandle::open();
    match handle_result {
        Ok(h) => xhandle = h,
        Err(e) => return Err(FomStartError::XrandrError(format!("{}", e))),
    }

    let monitors;
    let monitors_result = xhandle.monitors();

    match monitors_result {
        Ok(m) => monitors = m.iter().map(|x| { Monitor::from_xrandr_monitor(&mut xhandle, x) }).collect(),
        Err(e) => return Err(FomStartError::XrandrError(format!("{}", e))),
    }

    let disp = XDisplay::open_default()?;
    let w = disp.display_width();
    let h = disp.display_height();
    let w_mm = disp.display_width_mm();
    let h_mm = disp.display_height_mm();

    Ok(ScreenSettings{
        monitors,
        display: Span {
            dx: w,
            dy: h,
        },
        display_mm: Span {
            dx: w_mm,
            dy: h_mm,
        }
    })
}