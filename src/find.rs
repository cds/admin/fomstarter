use std::ops::Deref;
use crate::window::{XWindow, get_window_children, XDisplay};
use trees::{tr, Tree, Node};
use std::convert::From;

///
/// Recursively fill out tree starting at win_id as root
fn build_tree(display: &mut XDisplay, win_id: u64) -> Result<Tree<XWindow>, String> {


    let mut tree = tr(XWindow::new(display, win_id)?);
    let children = get_window_children(display, win_id);

    for child_id in children.iter() {
        let child = build_tree(display, *child_id)?;
        tree.push_back(child);
    }
    Ok(tree)
}

fn find_shortest_dist<'a>(x: i32, y: i32, root: &'a Node<XWindow>, path:& Vec<usize>) -> Vec<(Vec<usize>, &'a Node<XWindow>, f64)> {
    // let mut best_dist = root.data().distance(x,y);
    // let mut best_path: Vec<usize> = Vec::from(path.as_slice());
    // let mut best_win = root;

    let mut bests: Vec<(Vec<usize>, &'a Node<XWindow>, f64)> = Vec::new();

    

    for (i, child) in root.iter().enumerate() {
        let mut c_path = path.clone();
        c_path.push(i);
        let (path, win, dist) = find_shortest_dist(x,y, child, &c_path);
        if dist < best_dist {
            best_dist = dist;
            best_win = win;
            best_path = path;
        }
    }
    (best_path, best_win, best_dist)
}

pub fn find(win_id: u64) -> Result<(), String>{
    let mut display = XDisplay::open_default()?;
    let root = build_tree(& mut display, win_id)?;
    let node = root.deref();
    let (path, win_node, dist) = find_shortest_dist(1000,1000,node, &Vec::new());
    let win = win_node.data();
    println!("win: {} dist = {} path: {:?}", win, dist, path);
    Ok(())
}