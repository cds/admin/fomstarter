extern crate core;
extern crate shell_words;
extern crate pathfinding;

use std::env;
use std::path::{Path, PathBuf};
use std::process;
use std::u64;

use crate::coords::coords;
use crate::start::{start, generate, diff};
use thiserror::Error;
use crate::FomStartError::UnrecognizedCommand;

pub mod coords;
pub mod window;
pub mod start;
pub mod xdo;

// ///
// /// Try to determine a base before parsing.
// fn general_parse(value: &str) -> Result<u64, ParseIntError> {
//     let parse_value;
//     let radix;
//     if value.starts_with("0x") {
//         parse_value = &value[2..];
//         radix = 16;
//     }
//     else if value.starts_with("0b") {
//         parse_value = &value[2..];
//         radix = 2;
//     }
//     else {
//         parse_value = &value;
//         radix = 10;
//     }
//
//     u64::from_str_radix(parse_value, radix)
// }

fn usage(bin_name: & str) {
    println!("{} coords <window-id>", bin_name);
    println!("{} start <start-config-file>", bin_name);
    println!("{} diff <start-config-file>", bin_name);
    println!("{} generate [-u] [-r] [<start-config-file>]", bin_name);
    println!();
    // println!("\tfind: report the current mouse position and nearest subwindow to the mouse position");
    // println!("\t\tGet window id with wmctrl");

    println!("\tcoords:\treport the current mouse position relative to the given window");
    println!("\t\tGet window id with wmctrl");
    println!("\tstart:\tstart a FOM with the given yaml file as configuration");
    println!("\tdiff:\tfind differences in current windows and positions compared to a config file");
    println!("\t\tresult in a non-zero return code if any differences or problems are found");
    println!("\tgenerate: create a new start-up file from current screen settings");
    println!("\t\tor update an existing file with new window geometry");
    println!("\t\tif no file is given, then a new file is sent to stdout");
    println!("\t\t-u\tupdate only windows that are already in the file.  Don't add any new windows.");
    println!("\t\t-r\tremove any windows in the file that aren't open.");
    println!("\t\t-o\tforce output to stdout instead of updating file in place.");
    println!("\t\t-m\tdon't check monitors when generating a configuration.");
    println!("\t\t\tfor a new configuration, no monitor section will be generated");
    println!("\t\t\twhen updating, an existing monitor section will not be changed");

    println!();
    println!("{} version {}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));
}

// fn run_find(args: &[String]) -> Result<(), String>
// {
//     if args.len() == 1 {
//         let winid_str = args[0].as_str();
//         let winid_res = general_parse(winid_str);
//         match winid_res {
//             Ok(winid) => {find(winid)?; Ok(())},
//             Err(_) => Err(format!("window-id '{}' is not a number", winid_str)),
//         }
//     } else {
//         Err("wrong number of arguments".to_string())
//     }
// }

fn run_coords(_args: &[String]) -> Result<(), FomStartError>
{
    coords()
}

fn run_start(args: &[String]) -> Result<(), FomStartError> {
    if args.len() < 1 {
        Err(FomStartError::MissingArgument)
    }
    else{
        start(args[0].as_str())
    }
}

fn run_diff(args: &[String]) -> Result<(), FomStartError> {
    if args.len() < 1 {
        Err(FomStartError::MissingArgument)
    }
    else{
        diff(args[0].as_str())
    }
}

fn run_generate(args: &[String]) -> Result<(), FomStartError> {
    let mut yaml_file: Option<& str> = None;
    let mut update_only = false;
    let mut remove = false;
    let mut force_stdout = false;
    let mut include_monitors = true;
    for arg_str in args {
        let mut arg = arg_str.chars();
        let first_char = arg.next().unwrap();
        if first_char == '-' {
            if arg_str.len() <= 1 {
                return Err(FomStartError::UnrecognizedCommand(String::from("-")));
            }
            for c in arg {
                match c {
                    'u' => update_only = true,
                    'r' => remove = true,
                    'o' => force_stdout = true,
                    'm' => include_monitors = false,
                    _ => return Err(FomStartError::UnrecognizedCommand(String::from(c))),
                }
            }
        }
        else {
            yaml_file = Some(arg_str.as_str());
        }
    }
    generate(yaml_file, update_only, remove, force_stdout, include_monitors)
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let length = args.len();
    let bin_name = args[0].as_str();
    let mut return_code = Ok(());

    if length < 2 {
        usage(bin_name);
    }
    else {
        let command = args[1].as_str();
        let rest = &args[2..];

        match command {
            //"find" => return_code = run_find(rest),
            "coords" => return_code = run_coords(rest),
            "start" => return_code = run_start(rest),
            "diff" => return_code = run_diff(rest),
            "generate" => return_code = run_generate(rest),
            _ => return_code = Err(UnrecognizedCommand(command.to_string())),
        }
    }
    match return_code {
        Err(FomStartError::DifferenceWithConfig) => {
            println!("{}", FomStartError::DifferenceWithConfig.to_string());
            process::exit(1);
        },
        Err(e) => {
            println!("***ERROR {}\n", e);
            usage(bin_name);
            process::exit(1);
        },
        _ => (),
    }
}

/// return a list of configuration dirs in reverse priority
/// Overriding configuration from later directories should override
/// newer directories
pub fn config_dirs() -> Vec<PathBuf> {

    let mut conf_dirs = Vec::new();

    // get current exe working directory
    // backup for hand installed systems
    let ce = env::current_exe().unwrap();
    let ce_dir = ce.parent().unwrap();

    conf_dirs.push(ce_dir.into());

    // also try /etc/fomstarter/
    conf_dirs.push(PathBuf::from("/etc/fomstarter"));


    // try $home/.fomstarter
    if let Ok(pb) = std::env::current_dir() {
        conf_dirs.push(pb);
    }


    // try local directory
    if let Some(hd) = home::home_dir() {
        conf_dirs.push(hd.join(".fomstarter"));
    }
    conf_dirs
}

#[derive(Error, Debug)]
pub enum FomStartError  {
    #[error("Unrecognized command '{0}'")]
    UnrecognizedCommand(String),
    #[error("Unrecognized flag '{0}'")]
    UnrecognizedFlag(String),
    #[error("Could not open file {0}")]
    CantOpenFile(String),
    #[error("Could not write file {0}: {1}")]
    CantWriteFile(String, String),
    #[error("Could not parse yaml: {0}")]
    CantParseYaml(String),
    #[error("xrandr error: {0}")]
    XrandrError(String),
    #[error("Error getting window attributes for window {0:#x}")]
    GetWindowAttributesError(u64),
    #[error("Failed to open default display")]
    OpenDefaultDisplayError,
    #[error("Window name not found: id = {0:#x}")]
    GetWindowNameError(u64),
    #[error("Could not create XDo context")]
    CreateXDoError,
    #[error("Error getting atom name")]
    GetAtomNameError,
    #[error("Could not get a window property from {0:#x}")]
    GetWindowPropertyError(u64),
    #[error("Mis-matched monitor {0}")]
    UnspecifiedMonitors(String),
    #[error("Timeout waiting for window '{0}'")]
    WindowTimeout(String),
    #[error("Missing necessary command-line argument")]
    MissingArgument,
    #[error("More than one window opened while starting a new window '{0}'")]
    MultipleWindows(String),
    #[error("There was an error running the command to start window '{0}': {1}")]
    BadStartCommand(String, String),
    #[error("Differences were found between existing windows to a configuration file")]
    DifferenceWithConfig,
}
