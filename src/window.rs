use std::ptr;
use std::collections::{HashSet};
use std::convert::{TryFrom};
use x11::xlib;
use std::mem::MaybeUninit;
use std::ffi::{c_void, c_ulong, c_int, c_uchar, CStr, CString};
use std::ops::Drop;
use std::fmt::{Display, Formatter};
use crate::FomStartError;
use std::fs;
use std::time::Duration;
use std::thread;

use crate::xdo::XDo;

pub struct XDisplay {
    ptr: * mut xlib::Display,
}

impl XDisplay {
    pub fn open_default() -> Result<XDisplay, FomStartError> {
        let display_ptr = unsafe { xlib::XOpenDisplay(ptr::null()) };
        if !display_ptr.is_null() {
            Ok(XDisplay {
                ptr: display_ptr
            })
        } else {
            Err(FomStartError::OpenDefaultDisplayError)
        }
    }

    pub fn root(&self) -> u64 {
        unsafe{ xlib::XDefaultRootWindow(self.ptr) }
    }

    // list is returned in z-order from back to front, so the last window should be on top
    pub fn get_top_windows(& self) -> Vec<u64> {
        let windows: Vec<u64>;
        unsafe {
            let sorted_prop_atom = xlib::XInternAtom(self.ptr, "_NET_CLIENT_LIST_STACKING\0".as_ptr().cast(), 0);
            let unsorted_prop_atom = xlib::XInternAtom(self.ptr, "_NET_CLIENT_LIST\0".as_ptr().cast(), 0);
            let mut actual_type: c_ulong = 0;
            let mut actual_format: c_int = 0;
            let mut n: c_ulong = 0;
            let mut left_over: c_ulong = 0;
            let mut value_ptr: *mut c_uchar = ptr::null_mut();

            xlib::XGetWindowProperty(
                self.ptr,
                self.root(),
                sorted_prop_atom,
                0,
                1024,
                0,
                xlib::XA_WINDOW,
                &mut actual_type,
                &mut actual_format,
                &mut n,
                &mut left_over,
                &mut value_ptr
            );

            // if there's nothing in the STACKING list,
            // there may still be windows
            // X may not create the stacking list until two windows have overalpped.
            // If that hasn't happened, we don't care about the order of hte list.
            if n == 0 {
                xlib::XGetWindowProperty(
                    self.ptr,
                    self.root(),
                    unsorted_prop_atom,
                    0,
                    1024,
                    0,
                    xlib::XA_WINDOW,
                    &mut actual_type,
                    &mut actual_format,
                    &mut n,
                    &mut left_over,
                    &mut value_ptr
                );
            }

            if n > 0 {
                let word_ptr = value_ptr.cast();

                let word_slice = ptr::slice_from_raw_parts(word_ptr, usize::try_from(n).unwrap());
                let word_ref = &*word_slice;

                windows = Vec::from(word_ref);

                xlib::XFree(value_ptr.cast());
            }
            else {
                // no windows found
                windows = Vec::new();
            }
        }
        //get_window_children(self, self.root())
        windows
    }

    pub fn get_top_windows_as_set(& self) -> HashSet<u64> {
        let top_win_vec = self.get_top_windows();
        let mut top_win_set = HashSet::new();

        for id in top_win_vec {
            top_win_set.insert(id);
        }

        top_win_set
    }

    ///
    /// Wait until a new window appears, or timout.
    ///
    ///@param start_set, a set of windows ids produced by get_top_wiondows_as_set().
    /// When a new window appears that's not in this set, this function returns the id of the new window
    /// @param wait_time_sec timeout with an error when this many seconds has passed without a new window
    /// @param tag, short string used in error messages to identify the window being waited for.
    pub fn wait_for_new_window(&self, start_set: HashSet<u64>, wait_time_sec: u32, tag: &str) -> Result<u64, FomStartError> {
        let mut win_id :Option<u64> = None;
        let mut attempts = 0;
        while attempts < wait_time_sec && win_id.is_none() {
            attempts += 1;
            thread::sleep(Duration::from_secs(1));

            let win_set = self.get_top_windows_as_set();

            for &new_id in win_set.difference(&start_set) {
                let xdo = XDo::new().unwrap();
                let new_title = xdo.get_window_name(new_id).unwrap();

                // cut out medm extra windows
                if new_title != "Version" && new_title != "medm" &&
                    new_title.chars().take(6).collect::<String>() != String::from("caQtDM")
                {
                    match win_id {
                        None => win_id = Some(new_id),
                        Some(_) => return Err(FomStartError::MultipleWindows(String::from(tag))),
                    }
                }
            }
        }

        win_id.ok_or(FomStartError::WindowTimeout(String::from(tag)))
    }

    pub fn list_window_properties(&self, win_id: u64) {
        //let mut props;
        unsafe {
            let prop_ptr;
            let mut num_props: c_int = 0;
            prop_ptr = xlib::XListProperties(self.ptr, win_id, &mut num_props);

            let props_ptr = ptr::slice_from_raw_parts(prop_ptr, usize::try_from(num_props).unwrap());

            let props = &*props_ptr;

            for prop in props.iter() {
                let name = self.get_atom_name(*prop).unwrap();
                println!("{}\t{}", prop, name);
            }

            if num_props > 0 {
                xlib::XFree(prop_ptr.cast());
            }
        }
    }

    pub fn get_atom_name(&self, atom: u64) -> Result<String, FomStartError> {

        let name_ptr = unsafe{ xlib::XGetAtomName(self.ptr, atom) };
        if name_ptr.is_null() {
            return Err(FomStartError::GetAtomNameError);
        }
        let name_c = unsafe{ CStr::from_ptr(name_ptr) };

        let name = String::from(name_c.to_str().unwrap());

        unsafe{ xlib::XFree(name_ptr.cast()); }

        Ok(name)
    }

    pub fn display_width(& self) -> i32 {
        unsafe{ xlib::XDisplayWidth(self.ptr, 0) }
    }

    pub fn display_height(& self) -> i32 {
        unsafe{ xlib::XDisplayHeight(self.ptr, 0) }
    }

    pub fn display_width_mm(& self) -> i32 {
        unsafe{ xlib::XDisplayWidthMM(self.ptr, 0) }
    }

    pub fn display_height_mm(& self) -> i32 {
        unsafe{ xlib::XDisplayHeightMM(self.ptr, 0) }
    }
}

impl Drop for XDisplay {
    fn drop(&mut self) {
        assert!(!self.ptr.is_null());
        unsafe{ xlib::XCloseDisplay(self.ptr) };
    }
}

#[derive(Debug, Clone)]
pub struct XWindow {
    pub win_id: u64,
    pub attributes: xlib::XWindowAttributes,
}

impl XWindow {
    pub fn new(disp: & XDisplay, win_id: u64) -> Result<XWindow, FomStartError> {
    	let (ax, ay) = get_abs_pos(disp, win_id);
        let mut winatt = get_window_attributes(disp, win_id)?;

        winatt.x = ax;
        winatt.y = ay;

        Ok(XWindow {
            win_id,
            attributes: winatt,
        })

    }

    pub fn distance(& self, x: i32, y: i32) -> f64 {
        let dx = x - self.attributes.x;
        let dy = y - self.attributes.y;
        f64::from(dx*dx + dy*dy )
    }

    /// returns 0 if there's no discoverable PID
    pub fn pid(&self, disp: & XDisplay) -> u32 {
        let pid: u32;
        unsafe {
            let prop_atom = xlib::XInternAtom(disp.ptr, "_NET_WM_PID\0".as_ptr().cast(), 0);
            let mut actual_type: c_ulong = 0;
            let mut actual_format: c_int = 0;
            let mut n: c_ulong = 0;
            let mut left_over: c_ulong = 0;
            let mut value_ptr: *mut c_uchar = ptr::null_mut();
            xlib::XGetWindowProperty(
                disp.ptr,
                self.win_id,
                prop_atom,
                0,
                1024,
                0,
                0,
                &mut actual_type,
                &mut actual_format,
                &mut n,
                &mut left_over,
                &mut value_ptr
            );

            if n > 0 {
                pid = *value_ptr.cast();

                xlib::XFree(value_ptr.cast());
            } else {
                pid = 0;
            }
        }
        pid
    }

    pub fn command(&self, disp: & XDisplay) -> Result<String, FomStartError> {
        let pid = self.pid(disp);

        if pid == 0 {
            Err(FomStartError::GetWindowPropertyError(self.win_id))
        }
        else {
            get_command_from_pid(pid)
        }
    }

    pub fn set_title(&self, disp: &XDisplay, title: &str) -> i32 {
    	let ctitle = CString::new(title).unwrap();
    	unsafe{ xlib::XStoreName(disp.ptr, self.win_id, ctitle.as_ptr()) }
    }

    pub fn move_resize(&self, disp: & XDisplay, x: i32, y: i32, w: u32, h: u32) -> i32 {

        eprintln!("moveto: {} {}", x, y);

        // make sure it's not maximized

        self.demaximize(disp);

        // move to
    	let mv = unsafe{ xlib::XMoveResizeWindow(disp.ptr, self.win_id, x,y,w,h) };
        unsafe{ xlib::XFlush(disp.ptr); }
        thread::sleep(Duration::from_secs(1));
        let (ax, ay) = self.get_abs_pos(disp);
        let (dx, dy) = (ax - x, ay - y);

        if dx != 0 || dy != 0 {
            eprintln!("repos: x={} y={} ax={} ay={}", x,y,ax,ay);
            unsafe{ xlib::XMoveWindow(disp.ptr, self.win_id, x-dx, y-dy); }
            unsafe{ xlib::XFlush(disp.ptr); }
        }
        return mv;
    }

    pub fn demaximize(&self, disp: &XDisplay) {
        self.set_state(disp, 0, "_NET_WM_STATE_MAXIMIZED_HORZ");
        self.set_state(disp, 0, "_NET_WM_STATE_MAXIMIZED_VERT");
        //self.set_state(disp, 0, "MAXIMIZE_HORIZ");
        //self.set_state(disp, 0, "MAXIMIZE_VERT");
    }

    fn set_state(&self, disp: &XDisplay, action: i64, property: &str) {
        let net_wm_state = CString::new("_NET_WM_STATE").unwrap();
        let prop_cstring = CString::new(property).unwrap();

        let prop_atom = unsafe{ xlib::XInternAtom(disp.ptr, prop_cstring.as_ptr(), 1) };

        let wm_state_atom = unsafe{ xlib::XInternAtom( disp.ptr, net_wm_state.as_ptr() , 0) };

        let mut event: xlib::XEvent = unsafe{ std::mem::zeroed() };
        event.client_message.type_ = xlib::ClientMessage;
        event.client_message.serial = 0;
        event.client_message.send_event = 1;
        event.client_message.message_type = wm_state_atom;
        event.client_message.window = self.win_id;
        event.client_message.format = 32;

        unsafe {
            event.client_message.data.as_longs_mut()[0] = action;
            event.client_message.data.as_longs_mut()[1] = i64::try_from( prop_atom).unwrap();
        }

        unsafe{ xlib::XSendEvent(disp.ptr, xlib::XDefaultRootWindow(disp.ptr), 0, xlib::SubstructureNotifyMask, &mut event) };

        thread::sleep(Duration::from_secs(1));
    }


    pub fn get_abs_pos(&self, disp: &XDisplay) -> (i32, i32) {
        get_abs_pos(disp, self.win_id)
    }

    pub fn raise(&self, disp: &XDisplay) -> i32 {
        let gp = get_grandparent(disp, self.win_id);
        let net_active_win = CString::new("_NET_ACTIVE_WINDOW").unwrap();

        let mut event: xlib::XEvent = unsafe{ std::mem::zeroed() };
        event.client_message.type_ = xlib::ClientMessage;
        event.client_message.serial = 0;
        event.client_message.send_event = 1;
        event.client_message.message_type = unsafe{ xlib::XInternAtom( disp.ptr, net_active_win.as_ptr() , 0) };
        event.client_message.window = self.win_id;
        event.client_message.format = 32;

       unsafe{ xlib::XSendEvent(disp.ptr, xlib::XDefaultRootWindow(disp.ptr), 0, xlib::SubstructureRedirectMask | xlib::SubstructureRedirectMask, &mut event) };

        unsafe {xlib::XRaiseWindow(disp.ptr, gp)}
    }

    pub fn destroy(&self, disp: &XDisplay) -> i32 {
        unsafe{ xlib::XDestroyWindow(disp.ptr, self.win_id) }
    }
}

impl Display for XWindow {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "0x{:x} {},{} {}X{}", self.win_id, self.attributes.x, self.attributes.y, self.attributes.width, self.attributes.height)
    }
}

pub fn get_abs_pos(disp: &XDisplay, win_id: u64) -> (i32, i32) {
    let mut root: u64 = 0;
    // let mut parent: u64 = 0;
    // let mut children: *mut u64 = ptr::null_mut();
    // let mut num_child: u32 = 0;
    // let status = unsafe{ xlib::XQueryTree(disp.ptr, win_id, &mut root, &mut parent, &mut children, &mut num_child) };
    // unsafe{xlib::XFree(children as *mut c_void);}

    let mut x: i32=0;
    let mut y: i32=0;
    let mut w: u32=0;
    let mut h: u32=0;
    let mut b: u32=0;
    let mut d: u32=0;
    unsafe { xlib::XGetGeometry(disp.ptr, win_id, & mut root, &mut x, &mut y, &mut w, &mut h, &mut b, &mut d); }

    let mut abs_x: i32=0;
    let mut abs_y: i32=0;
    let mut child: u64=0;
    unsafe { xlib::XTranslateCoordinates(disp.ptr, win_id, root, 0, 0, &mut abs_x, &mut abs_y, &mut child); }
    (abs_x, abs_y)
}

pub fn print_grandparent(disp: &XDisplay, win_id: u64) -> u64 {
    let mut root: u64 = 0;
    let parent: u64 = 0;

    let mut x: i32=0;
    let mut y: i32=0;
    let mut w: u32=0;
    let mut h: u32=0;
    let mut b: u32=0;
    let mut d: u32=0;
    unsafe { xlib::XGetGeometry(disp.ptr, win_id, & mut root, &mut x, &mut y, &mut w, &mut h, &mut b, &mut d); }
    eprintln!("win geom: {}: x={} y={} w={} h={} b={} d={}", win_id, x,y,w,h,b,d);

    let (ax, ay) = get_abs_pos(disp, win_id);
    eprintln!("abs: x={} y={}", ax, ay);

    if parent == root {
    	    unsafe { xlib::XGetGeometry(disp.ptr, parent, & mut root, &mut x, &mut y, &mut w, &mut h, &mut b, &mut d); }
            eprintln!("rootgeom: {}: x={} y={} w={} h={} b={} d={}", parent, x,y,w,h,b,d);
	    win_id
    }
    else {
	    print_grandparent(disp, parent)
    }
}

pub fn get_grandparent(disp: &XDisplay, win_id: u64) -> u64 {
    let root: u64 = 0;
    let parent: u64 = 0;
    let children: *mut u64 = ptr::null_mut();

    unsafe{xlib::XFree(children as *mut c_void);}

    if parent == root {
	    win_id
    }
    else {
	    get_grandparent(disp, parent)
    }
}

pub fn get_window_attributes(display: & XDisplay, winid: u64) -> Result<xlib::XWindowAttributes, FomStartError> {
    let mut win_att_init: MaybeUninit<xlib::XWindowAttributes> = MaybeUninit::uninit();
    unsafe {
        let status = xlib::XGetWindowAttributes(display.ptr, winid,win_att_init.as_mut_ptr());
        if status != 0 {
            Ok(win_att_init.assume_init())
        }
        else {
            Err(FomStartError::GetWindowAttributesError(winid))
        }
    }

}

pub fn get_window_children(display: & XDisplay, win_id: u64) -> Vec<u64> {
    unsafe {
        let mut root: u64 = 0;
        let mut parent: u64 = 0;
        let mut children: *mut u64 = ptr::null_mut();
        let mut num_child: u32 = 0;
        let status = xlib::XQueryTree(display.ptr, win_id,&mut root, &mut parent, &mut children, &mut num_child);
        if status != 0 && !children.is_null() && num_child > 0 {
            let child_void = children as *mut c_void;
            let slice = ptr::slice_from_raw_parts(children, usize::try_from(num_child).unwrap_or(0));
            let mut child_vec = Vec::new();
            for child in (*slice).iter() {
                child_vec.push(*child);
            }
            xlib::XFree(child_void);
            child_vec
        }
        else {
            vec!()
        }
    }
}

pub fn get_command_from_pid(pid: u32) -> Result<String, FomStartError> {
    let fname = format!("/proc/{}/cmdline", pid);
    let read = fs::read_to_string(fname.as_str());

    match read {
        Ok(x) => Ok(x),
        Err(_) => Err(FomStartError::CantOpenFile(fname)),
    }
}


// fn get_mouse_pos(display: & mut XDisplay) -> (x: i32, y: i32) {
//     xlib::
// }