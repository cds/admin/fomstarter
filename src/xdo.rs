use libxdo_sys::{xdo_new, xdo_free, xdo_t, xdo_get_window_name};
use crate::FomStartError;
use std::ffi::{c_uchar, c_int};
use std::ptr;
use std::convert::{TryFrom};
use std::io::Read;
use std::ops::Drop;

pub struct XDo {
    pub ptr: * mut xdo_t,
}

impl XDo {
    pub fn get_window_name(&self, win_id: u64) -> Result<String, FomStartError> {

        let mut chars;
        unsafe {
            let mut name_ret: * mut c_uchar = ptr::null_mut();
            let mut name_len: c_int = 0;
            let mut name_type: c_int = 0;
            let result = xdo_get_window_name(self.ptr, win_id, & mut name_ret, & mut name_len, & mut name_type);
            if result != 0 || name_type == 0 || name_len == 0 {
                return Err(FomStartError::GetWindowNameError(win_id));
            }

            let chars_ptr = ptr::slice_from_raw_parts(name_ret, usize::try_from(name_len).unwrap());
            //Ok(String::from(*chars))
            // let chars_vec = (*chars).into_iter().collect();
            chars = &*chars_ptr;


        }
        let mut name: String = String::new();
        chars.read_to_string(& mut name).unwrap();

        Ok(name)
    }

    pub fn new() -> Result<XDo, FomStartError> {
        let xdo_ptr: *mut xdo_t;
        unsafe{
            xdo_ptr = xdo_new(ptr::null());
            if xdo_ptr.is_null() {
                return Err(FomStartError::CreateXDoError);
            }
        }
        Ok(XDo {
            ptr: xdo_ptr,
        })
    }
}

impl Drop for XDo {
    fn drop(&mut self) {
        unsafe{ xdo_free(self.ptr) };
    }
}