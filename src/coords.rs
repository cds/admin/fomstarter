use libxdo_sys::{xdo_new, xdo_get_mouse_location2, xdo_get_window_location};
use std::ffi::{c_int};
use std::ptr;
use std::thread::sleep;
use std::time::Duration;
use crate::FomStartError;
//use crate::window::{XWindow, get_window_children, XDisplay};

pub fn coords() -> Result<(), FomStartError> {
    let xdo = unsafe{ xdo_new(ptr::null()) };
    let mut x: c_int = 0;
    let mut y: c_int = 0;
    let mut screen: c_int = 0;
    let mut win: u64 = 0;
    loop {
        unsafe{ xdo_get_mouse_location2(xdo, &mut x, &mut y,  &mut screen, &mut win);}

        let mut x_win: i32 = 0;
        let mut y_win: i32 = 0;
        if win > 0 {
            unsafe { xdo_get_window_location(xdo, win, &mut x_win, &mut y_win, ptr::null_mut()); }

            let r_x = x - x_win;
            let r_y = y - y_win;
            println!("{}, {}, {}, {:#x}", r_x, r_y, screen, win);
        }
        sleep(Duration::new(0,100000000));
    }

    // unsafe{ xdo_free(xdo); }
    // Ok(())
}