#!/bin/bash


cargo clean
cargo build --release

cp target/release/fomstarter /ligo/apps/debian10/utils/bin/
cp src/click_macros.yaml /ligo/apps/debian10/utils/bin/
